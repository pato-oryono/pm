import {Routes, RouterModule} from "@angular/router";
import {LoginComponent} from "./login/login.component";
import {NgModule} from "@angular/core";
import {AuthGuard} from "../guards/auth.guard";
import {RegisterComponent} from "./register/register.component";
import {HomeComponent} from "./dashboard/home/home.component";
import {BranchesComponent} from "./dashboard/branches/branches.component";
import {NotificationsComponent} from "./dashboard/notifications/nofications.component";
const routes: Routes= [
  { path: 'dashboard',  component: HomeComponent },
  { path: 'branches',  component: BranchesComponent },
  { path: 'login',  component: LoginComponent },
  { path: 'register',  component: RegisterComponent },
  { path: 'notifications',  component: NotificationsComponent },
  { path: '**', redirectTo: 'dashboard' },

]

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})



export class AppRoute{}
