import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import {NavigationComponent} from "./dashboard/navigation/navigation.component";
import {LoginComponent} from "./login/login.component";
import {AppRoute} from "./app.routing";
import {RegisterComponent} from "./register/register.component";
import {AuthGuard} from "../guards/auth.guard";
import {SidebarComponent} from "./sidebar/sidebar.component";
import {HomeComponent} from "./dashboard/home/home.component";
import {BranchesComponent} from "./dashboard/branches/branches.component";
import {NotificationsComponent} from "./dashboard/notifications/nofications.component";

@NgModule({
  declarations: [
    AppComponent,
    NavigationComponent,
    LoginComponent,
    RegisterComponent,
    SidebarComponent,
    HomeComponent,
    BranchesComponent,
    NotificationsComponent

  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    AppRoute
  ],
  providers: [AuthGuard],
  bootstrap: [AppComponent]
})
export class AppModule {}
